#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import os.path

from gdl_compiler import PropNet
from gdl_lexer import gdl_lexer
from gdl_parser import gdl_parser


if __name__ == '__main__':
    pathname = sys.argv[1]
    
    filename = os.path.basename(pathname); #print(filename)
    base, _, ext = filename.partition('.'); #print(base)

    propnet = PropNet(pathname)

    k = len(propnet.G.nodes())
    print(k, 'nodes in propnet')
    
    ans = input('générer propnet_' + base + '.cpp ? (y / n) : ')
    if ans[0].upper() == 'Y':
        propnet.generate(base)

    ans = input('générer le graphe ? (y / n) : ')
    if ans[0].upper() == 'Y':
        propnet.to_dot(base + '.dot')

