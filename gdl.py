#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from uniq import utuple, ustring

class Rule(utuple):
    def __repr__(self):
        return repr(self.head) + ':-' + ','.join(repr(clause) for clause in self.body) + '.'
    head = property(fget=lambda self: self[0])
    body = property(fget=lambda self: self[1:])

class Fact(utuple):
    def __repr__(self):
        return repr(self.head) + '.'
    head = property(fget=lambda self: self[0])

class Compound(utuple):
    def __repr__(self):
        return self.functor + '(' + ','.join(repr(arg) for arg in self.args) + ')'
    def to_kif(self):
        return '(' + self.functor + ' ' + ' '.join(arg.to_kif() for arg in self.args) + ')'
    def __lt__(self, other):
        if isinstance(other, Atom):
            return False
        elif isinstance(other, Compound):
            return utuple.__lt__(self, other)
        else:
            raise TypeError('unorderable types: {} and {}'.format(Compound, type(other)))
    arity = property(fget=lambda self: len(self[1:]))            
    functor = property(fget=lambda self: self[0])
    args = property(fget=lambda self: self[1:])

class Atom(ustring):
    def __repr__(self):
        return self
    def to_kif(self):
        return self
    def __lt__(self, other):
        if isinstance(other, Atom):
            return str.__lt__(self, other)
        elif isinstance(other, Compound):
            return True
        else:
            raise TypeError('unorderable types: {} and {}'.format(Atom, type(other)))
    functor = property(fget=lambda self: self)
