#!/usr/bin/env python3
# -*- coding: utf-8 -*-


from gdl_parser import Compound, Fact, Rule
from logic import *

def gensym(nd):
    if isinstance(nd, Not):
        return 'not_' + str(id(nd))
    elif isinstance(nd, And):
        return 'and_' + str(id(nd))
    elif isinstance(nd, Or):
        return 'or_' + str(id(nd))
    else:
        return '"' + repr(nd) + '"'

##def gensym(nd):
##    if isinstance(nd, (Not, Or, And)):
##        return gensym(repr(nd))
##    else:
##        return nd.replace("'", '').replace(',','_').replace('(','K_').replace(')','_X').replace(' ', '').replace('~', 'NOT_').replace('&', '_AND_').replace('|', '_OR_')


def to_dot(G, filename, print_transitions=True):
    """
    dct: les règles stockées dans un dict au format DNF
    filename: le nom du fichier .dot en sortie
    """
    visited = set()
    fp = open(filename, 'w')

    def rec_visit(nd):
        if nd in visited:
            return
        else:
            visited.add(nd)

        uid = gensym(nd)

        # add node
        if isinstance(nd, Or):
            fp.write(' {} [shape=ellipse, style=filled, fillcolor=grey, label="OR"];\n'.format(uid))
        elif isinstance(nd, And):
            fp.write(' {} [shape=invhouse, style=filled, fillcolor=grey, label="AND"];\n'.format(uid))
        elif isinstance(nd, Not):
            fp.write(' {} [shape=invtriangle, style=filled, fillcolor=grey, label="NOT"];\n'.format(uid))
        else:
            fp.write(' {} [style=filled, fillcolor=white, label="{}"];\n'.format(uid, nd))

        # add edges
        if isinstance(nd, Logic):
            for nnd in nd:
                uuid = gensym(nnd)
                fp.write(' {} -> {};\n'.format(uuid, uid))
                rec_visit(nnd)
        else:
            prev = G.predecessors(nd)[0]
            if prev != True:
                uuid = gensym(prev)
                fp.write(' {} -> {};\n'.format(uuid, uid))
                rec_visit(prev)
            elif nd.functor == 'true' and print_transitions:
                prev = nd.transition
                uuid = gensym(prev)
                fp.write(' {} -> {} [color=blue, style=dashed];\n'.format(uuid, uid))
                #print(' "{}" -> "{}";'.format(uuid, uid))

    fp.write('digraph propNet\n{\n')

    lst = [node for node in G.nodes() if hasattr(node, 'functor')]
    for node in lst:
        if node.functor in {'true', 'does', 'next', 'legal', 'goal', 'terminal'}:
            rec_visit(node)

    fp.write(' {rank=source; ' + '; '.join(gensym(n) for n in lst if n.functor == 'does') + ';}\n')
    fp.write(' {rank=source; ' + '; '.join(gensym(n) for n in lst if n.functor == 'true') + ';}\n')
    fp.write(' {rank=same; ' + '; '.join(gensym(n) for n in lst if n.functor == 'goal') + ';}\n')
    fp.write(' {rank=same; ' + '; '.join(gensym(n) for n in lst if n.functor == 'legal') + ';}\n')
    fp.write(' {rank=same; ' + '; '.join(gensym(n) for n in lst if n.functor == 'terminal') + ';}\n')
    if print_transitions:
        fp.write(' {rank=same; ' + '; '.join(gensym(n) for n in lst if n.functor == 'next') + ';}\n')

    fp.write('}\n')
    fp.close()
