#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
from pprint import pprint
from collections import defaultdict
from itertools import chain, count


import networkx as nx

from gdl_lexer import gdl_lexer
from gdl_parser import gdl_parser, Atom, Compound, Fact, Rule
from logic import *
from gdl2dot import to_dot
from propnet_skeletons import *


# parse un fichier de règles instanciées (et désaxiomatisées), 
# et retourne une liste de règles.
# actuellement, les règles doivent être exclusivement conjonctives
# (ce qui est généralement fait au niveau de la désaxiomatisation),
# pour gérer les disjonctions à l'intérieur des clauses il faut 
# étendre le parser.
def bar(filename):
    with open(filename, 'r') as fp:
        return gdl_parser.parse(fp.read(), lexer=gdl_lexer)


def gensym(elt):
    return repr(elt)
    if isinstance(elt, Logic):
        return '$' + str(next(gensym.gen))
    else:
        return repr(elt)
gensym.gen = count(1)

# récupère les règles instanciées en Dijunctive Normal Norm, ie.
#   p :- q.
#   p :- r.
# devient :
#   p :- q; r.
def foo(filename):
    G = nx.DiGraph()
    gdl = bar(filename)
    
    dct = {}
    for entry in gdl:
        #print(entry)
        assert isinstance(entry.head, Prop)
        if isinstance(entry, Fact):
            dct[entry.head] = None
        elif isinstance(entry, Rule):
            assert all(isinstance(elt, (Not, Prop)) for elt in entry.body)
            dct.setdefault(entry.head, []).append(And(*entry.body))
        else:
            raise RuntimeError('unexcepted entry !')

    dct2 = {}
    for head, lst in dct.items():
        #print(head); input('continuer ?')
        prev = Or(*lst).simplify() if lst else False
        dct2[head] = prev
        if prev:
            head.depend = prev
    
    visited = set()
    def rec_traversal(nd):
        #print('visit', nd)
        if nd in visited:
            return
        visited.add(nd)
        G.add_node(nd, label=gensym(nd))
        
        if isinstance(nd, (Not, And, Or)):
            prevs = list(nd)
        else:
            prevs = [dct2[nd]] if dct2[nd] != False else []
        #print('  prevs: ', prevs)
        for prev in prevs:
            G.add_edge(prev, nd)
            rec_traversal(prev)

    for nd, prev in dct2.items():
        rec_traversal(nd)
        
    # nx_to_dot(G, 'test.dot')

    return G

def nx_to_dot(G, filename):
    #TODO: ajouter une visualisation plus claire
    with open(filename, 'w') as fp:
        fp.write('digraph propNet\n{\n')
        for nd in G.nodes():
            label = gensym(nd)
            fp.write(' "{}";\n'.format(label))
        for u, v in G.edges():
            lu, lv = gensym(u), gensym(v)
            fp.write(' "{}" -> "{}";\n'.format(lu, lv))
        fp.write('}\n')


class PropNet:
    def __init__(self, filename):
        self.G = foo(filename)
        
        self.nodes = PropNet.classify(self.G.nodes())

        self.init = self.nodes['init']
        self.roles = self.nodes['role']
        #print(self.roles)
        
        self.lbase = []
        self.linput = []

        # marque l'état de départ
        for nd in self.nodes['base']:
            if nd.term.args in self.init:
                nd.val = True
                #self.lbase.append(nd)

        self.first_compute() # pour avoir la légalité

    @staticmethod
    def classify(nodes):
        dct = {k: [] for k in ['base', 'input', 'next', 'legal', 'terminal', 'goal', 'role', 'view']}
        dct['init'] = set()
        for prop in nodes:
            if not isinstance(prop, Prop):
                continue
            nd = prop.term
            if nd.functor == 'role':
                dct['role'].append(nd.args[0])
            if nd.functor == 'true':
                dct['base'].append(prop)
            elif nd.functor == 'does':
                dct['input'].append(prop)
            elif nd.functor == 'init':
                # ajoute les arguments pour comparer plus simplement
                dct['init'].add(nd.args)
            elif nd.functor == 'next':
                dct['next'].append(prop)
            elif nd.functor == 'legal':
                dct['legal'].append(prop)
            elif nd.functor == 'goal':
                dct['goal'].append(prop)
            elif nd.functor == 'terminal':
                dct['terminal'].append(prop)
            else:
                dct['view'].append(prop) # for debug

        for k in ['base', 'next', 'input', 'legal', 'goal']:
            dct[k].sort(key=lambda x: x.term)
        
        # TODO: mettre plutot les transitions dans le graphe ? (impossible, ce ne serait plus un DAG)
        # TODO: utiliser des pseudo-transitions s'il n'en existe pas
        trans = {nd.term.args: nd for nd in dct['next']}
        for nd in dct['base']:
            nd.transition = trans.get(nd.term.args)

        return dct

    def first_compute(self):
        nbunch = set(chain(*[self.G.predecessors(nd) 
                        for nd in self.nodes['legal'] + self.nodes['terminal'] + self.nodes['goal']]))
        #print(len(nbunch), nbunch); input('continuer ?')
        
        order = nx.topological_sort(self.G, nbunch)
        
        # if faut rattacher les propositions à leur dépendance...
        for nd in order:
            v = nd.eval()
            # print(nd, v); # input('continuer?')
            #if (v): print(nd, v)
        
        self.first = True

    def compute(self, inputs):
        # marking input
        for nd in self.linput:
            nd.val = False
        self.linput.clear()
        for nd in inputs:
            nd.val = True
        self.linput = inputs
        
        nbunch = set(chain(*[self.G.successors(nd) for nd in inputs]))
        print('compute', len(nbunch)); # input('continuer ?')
        
        order = nx.topological_sort(self.G, nbunch)
        
        # if faut rattacher les propositions à leur dépendance...
        for nd in order:
            v = nd.eval()
            #print(nd, v); input('continuer?')
            #if (v): print(nd, v)

    def next_state(self, inputs):
        self.compute(inputs)
        self.make_transitions()
        
    def make_transitions(self):
        for nd in self.nodes['base']:
            if hasattr(nd, 'transition'):
                nd.val = nd.transition.val
            else:
                nd.val = False

        nbunch = set(chain(*[self.G.successors(nd) for nd in self.nodes['base']]))
        print('trans', len(nbunch)); # input('continuer ?')
        
        order = nx.topological_sort(self.G, nbunch)
        
        # if faut rattacher les propositions à leur dépendance...
        for nd in order:
            v = nd.eval()

    def get_current_state(self, key):
        return sorted((prop for prop in self.nodes[key] if prop.val), key=lambda e: id(e))

    def debug(self):
        for key in ['base', 'input', 'view', 'legal', 'goal', 'terminal', 'next']:
            print(key, ':', self.get_current_state(key))
        print(50 * '-')

    def to_dot(self, filename):
        to_dot(self.G, filename)
