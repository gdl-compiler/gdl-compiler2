#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import ply.lex as lex

# List of token names.
tokens = ['ATOM', 'IMPLIES']
literals = ['(', ')', ',', '.', '~']


# A string containing ignored characters (spaces and tabs)
t_ignore_SPACE = r'\s+'
t_ignore_COMMENT = r';.*'
t_ignore_NEWLINE = r'\n'

def t_ATOM(t):
    r'[0-9A-Za-z_]+'
    return t

def t_IMPLIES(t):
    r':-'
    return t

# Error handling rule
def t_error(t):
    raise RuntimeError("Illegal character '%s' at line %d" % (t.value[0], t.lexer.lineno))

# Build the lexer
gdl_lexer = lex.lex(optimize=1)
