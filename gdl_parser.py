#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import ply.yacc as yacc


# Get the token map from the lexer.
from gdl_lexer import tokens
from gdl import *
from logic import Not, Prop


def p_source(p):
    """
    source :
           | source term
    """
    if len(p) == 1:
        p[0] = []
    elif len(p) == 3:
        p[1].append(p[2])
        p[0] = p[1]

def p_term(p):
    """
    term : expr IMPLIES seq '.'
         | expr '.'
    """
    if len(p) == 5:
        p[0] = Rule([p[1]] + p[3])
    elif len(p) == 3:
        p[0] = Fact([p[1]])
    
def p_expr(p):
    """
    expr : atom
         | compound
         | '~' expr
    """
    if len(p) == 2:
        p[0] = Prop(p[1])
    elif len(p) == 3:
        p[0] = Not(p[2])  # correct ?

def p_compound(p):
    """
    compound : atom '(' seq ')'
    """
    atom = p[1]
    # hack (for easier topological sorting)
    if atom == 'base':
        atom = 'true'
    elif atom == 'input':
        atom = 'does'
    p[0] = Compound([atom] + p[3])

def p_atom(p):
    """
    atom : ATOM
    """
    p[0] = Atom(p[1])

def p_seq(p):
    """
    seq : expr
        | seq ',' expr
    """
    if len(p) == 2:
        p[0] = [p[1]]
    elif len(p) == 4:
        p[1].append(p[3])
        p[0] = p[1]


# Error rule for syntax errors
def p_error(p):
    raise RuntimeError('\tin ' + repr(p) + '\n\tSyntax error in input !')


# Build the parser
gdl_parser = yacc.yacc(optimize=1)
