#!/usr/local/bin/yap -L --
#.

:- use_module(library(apply_macros)).
:- use_module(library(readutil)).
:- use_module(library(lists)).


% GDL builtins
:- op(900, fy, ~).


~(Term) :- 
    ( \+ ground(Term) ->
      throw('Argl') ;
      true ). % hack : no use negation; this is dirty...


distinct(X,Y) :- 
    X \= Y.

% very unsafe mode...
or(X,Y) :- 
    X ; Y.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

:- table true/1, does/2.

% additional predicates
true(X) :- base(X).
does(X,Y) :- input(X,Y).

rewrite((Head :- Body)) :- !,
    call(Body).
rewrite(Fact).

writeln_dot(Term) :-
    write(Term), put_char('.'), nl.
    
analyse_term(Term) :-
    forall(rewrite(Term), writeln_dot(Term)).

analyse_all(Filename) :-
    consult(Filename),
    read_file_to_terms(Filename, [Tab|L], []),
    maplist(analyse_term, L).

main([]).
main([H|T]) :-
        analyse_all(H),
        main(T).

:- unix(argv(AllArgs)), main(AllArgs).

