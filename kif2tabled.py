#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
from time import time

from kif_lexer import kif_lexer
from kif_parser import kif_parser
from kif import *

if __name__ == '__main__':
    filename = sys.argv[1]
    content = open(filename, 'r').read()
    tabled = set()
    lst = kif_parser.parse(content, lexer=kif_lexer)
    for elt in lst:
        if isinstance(elt, Rule):
            tabled.add(repr(elt))
    print(':- table', ', '.join(str(e) for e in tabled), '.\n')
    for elt in lst:
        print(elt.to_gdl())
