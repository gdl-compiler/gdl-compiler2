#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import ply.lex as lex


reserved = {
   # 'nil' : 'NIL',
    'not' : 'NOT',
    '<=' : 'IMPLIES'
}

# List of token names.
tokens = [
    'SYMBOL',
    'INT',
    'VAR'
] + list(reserved.values())

literals = ['(', ')']



# A string containing ignored characters (spaces and tabs)
t_ignore_SPACE = r'\s+'
t_ignore_COMMENT = r';.*'

# Define a rule so we can track line numbers
def t_newline(t):
    r'\n'
    t.lexer.lineno += len(t.value)

def t_INT(t):
    r'\d+'
    t.value = int(t.value)
    return t

def t_SYMBOL(t):
    r'([A-Za-z_][0-9A-Za-z_]*)|(<=)'
    t.type = reserved.get(t.value, 'SYMBOL')    # Check for reserved words
    return t

def t_VAR(t):
    r'[?][A-Za-z_][0-9A-Za-z_]*'
    t.value = t.value[1:]
    return t

# Error handling rule
def t_error(t):
    raise RuntimeError("Illegal character '%s' at line %d" % (t.value[0], t.lexer.lineno))

# Build the lexer
kif_lexer = lex.lex()

