#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#TODO: adding more simplifications rules

from functools import reduce
from uniq import utuple

def concat(*lists):
    return reduce(lambda x, y: x + y, lists)

class Logic(utuple):
    def __new__(cls, *args):
        if cls in {Or, And}:
            seq = sorted(set(args), key=id) # sorting by id (assume Logic is uniq)
        else:
            seq = args
        return super().__new__(cls, seq)

class Prop(Logic):
    def __init__(self, *args):
        self.val = False
        assert len(args) == 1
    def eval(self):
        if hasattr(self, 'depend'):
            self.val = self.depend.val  # don't eval() twice...
        return self.val
    def __repr__(self):
        return repr(self.term)
    def simplify(self):
        return self
    term = property(fget=lambda self: self[0])

class Not(Logic):
    def __init__(self, *args):
        self.val = False
        assert len(args) == 1
    def eval(self):
        self.val = not self.term.eval()
        return self.val
    def __repr__(self):
        return '~' + repr(self.term)
    def simplify(self):
        # double negation
        if isinstance(self.term, Not):
            tt = self.term.term
            return tt.simplify() if isinstance(tt, Logic) else tt
        elif isinstance(self.term, Logic):
            content = self.term.simplify()
            nb_not = sum(1 if isinstance(nd, Not) else 0 for nd in content)
            if 2 * nb_not < len(content):
                return Not(content)
            else:
                constructor = {And: Or, Or: And}[type(content)]
                return constructor(*[Not(nd) for nd in content]).simplify()
        else:
            return self
    term = property(fget=lambda self: self[0])

def binarize(foo, it):
	if len(it) == 1:
		return it[0]
	else:
		return foo(it[0], binarize(foo, it[1:]))
        
def bin_logic(logic):
    if isinstance(logic, (And, Or)):
        return binarize(type(logic), [bin_logic(nd) for nd in logic])
    else:
        return logic

class And(Logic):
    def __init__(self, *args):
        self.val = False
    def eval(self):
        self.val = all(prop.eval() for prop in self)
        return self.val
    def __repr__(self):
        return '(' + ' & '.join(repr(c) for c in self) + ')'
    def simplify(self):
        if len(self) == 1:
            return self[0].simplify() if isinstance(self[0], Logic) else self[0]
        else:
            content = [nd.simplify() if isinstance(nd, Logic) else nd for nd in self]
            if all(isinstance(obj, And) for obj in content):
                content = concat(*content)
            return And(*content)

class Or(Logic):
    def __init__(self, *args):
        self.val = False
    def eval(self):
        self.val = any(prop.eval() for prop in self)
        return self.val
    def __repr__(self):
        return '(' + ' | '.join(repr(c) for c in self) + ')'
    def simplify(self):
        if len(self) == 1:
            return self[0].simplify() if isinstance(self[0], Logic) else self[0]
        else:
            content = [nd.simplify() if isinstance(nd, Logic) else nd for nd in self]
            if all(isinstance(obj, Or) for obj in content):
                content = concat(*content)
            return Or(*content)


class Eqv(Logic):
    def __init__(self, *args):
        assert len(args) == 2
    def __repr__(self):
        return '(' + repr(self.a) + ' <=> ' + repr(self.b) + ')'
    def simplify(self):
        return And(Implies(self.a,self.b), Implies(self.b,self.a)).simplify()
    a = property(fget=lambda self: self[0])
    b = property(fget=lambda self: self[1])

    
class Implies(Logic):
    def __init__(self, *args):
        assert len(args) == 2
    def __repr__(self):
        return '(' + repr(self.a) + ' => ' + repr(self.b) + ')'
    def simplify(self):
        sb = self.b.simplify() if isinstance(self.b, Logic) else self.b
        return Or(Not(self.a), sb).simplify()
    a = property(fget=lambda self: self[0])
    b = property(fget=lambda self: self[1])
