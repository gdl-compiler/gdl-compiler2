#!/usr/local/bin/yap -L --
#.

:- use_module(library(lists)).
:- use_module(library(apply_macros)).
:- use_module(library(readutil)).


:- op(900, fy, ~).

distinct(A, B) :- true. % A \= B.

:- table gdl/1.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% règles pour réduire le or
reduce(or(X, Y), true) :-
    reduce(X, true), !.
reduce(or(X, Y), true) :-
    reduce(Y, true), !.
reduce(or(X, Y), or(ResX, ResY)) :- !,
    reduce(X, ResX),
    reduce(Y, ResY).

% reduire une conjonction
reduce((A, B), Res) :- !,
    reduce(A, Pa),
    reduce(B, Pb),
    combine(Pa, Pb, Res).

reduce(A, true) :-
    call(A), !.

% par default
reduce(A, A).

combine(true, B, B):- !.
combine(A, true, A):- !.
combine(A, B, (A, B)).


gdl(Term) :-
    memberchk(Term, [role(_), base(_), init(_), input(_,_), legal(_,_), goal(_,_), next(_), terminal]).


% for rules
reduceTerm((H :- B), O) :- !,
    % write('Entry(rule) :'), write(H), nl,
    ( reduce(B, true) ->
      ( gdl(H) ->  
        O = H ;
        O = true );
      ( reduce(B, R),
        O = (H :- R) ) ).

% for facts
reduceTerm(H, R) :-
    % write('Entry(fact) :'), write(H), nl,
    ( gdl(H) ->
      R = H ;
      R = true ).



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


filter([Tx|Ts], Res) :-
    reduceTerm(Tx, true), !,
    filter(Ts, Res).
filter([Tx|Ts], [NewTerm|Res]) :- !,
    reduceTerm(Tx, NewTerm),
    filter(Ts, Res).
filter([],[]).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

wr_term(Term) :-
    write(Term), put_char('.'), nl.

analyse_all(Filename) :-
    consult(Filename),
    read_file_to_terms(Filename, Prog, []),
    filter(Prog, FProg),
    remove_duplicates(FProg, OProg),
    maplist(wr_term, OProg).

    
main([]).
main([H|T]) :-
        analyse_all(H),
        main(T).

% :- source.  % pour pouvoir utiliser clause/2
:- unix(argv(AllArgs)), main(AllArgs).
