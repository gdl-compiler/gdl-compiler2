class utuple(tuple):
    __slots__ = []
    cache = {}
    def __new__(cls, it):
        # assert not isinstance(it, utuple)
        tp = tuple(it)
        key = cls, tp
        if key in utuple.cache:
            return utuple.cache[key]
        ret = super().__new__(cls, it)
        utuple.cache[key] = ret
        return ret
    def __eq__(self, other):
        return self is other
    def __hash__(self):
        return id(self)
        
class ustring(str):
    __slots__ = []
    cache = {}
    def __new__(cls, s):
        if s in ustring.cache:
            return ustring.cache[s]
        ret = super().__new__(cls, s)
        ustring.cache[s] = ret
        return ret
